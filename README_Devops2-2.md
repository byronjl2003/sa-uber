# Documentacion Tarea Devops 2/2
# Softaware avanzado - USAC

### Byron Jose Lopez Herrera
### 201222626
### REPO: https://github.com/byronjl2003/SA_Tarea5

## Instrucciones Generales
- Archivo para generar el artefacto: gulpfile.js
-Sitio de Netlify: https://app.netlify.com/sites/sa-201222626/deploys

- Descarga de artefacto
![artefacto][logo]

[logo]: https://github.com/byronjl2003/SA_Tarea2/blob/master/diagramas/solicitud-servicio.png?raw=true "artefacto"
